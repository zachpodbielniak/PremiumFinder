# Premium Finder
A tool that uses the TastyTrade API to find some good premium using various strategies.

As of right now the only strategy that is supported is the csp_strategy. 

## Running
Be sure to create an account_secrets.py folder under the Src/ folder.
```
python3 ./Src/csp_strategy.py
```

See the **--help** option for more info and tunable configs.
