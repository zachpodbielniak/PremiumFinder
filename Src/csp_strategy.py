from structures import CspData
from runtime import init_runtime
from helper import get_symbols_list, get_equity_quotes, get_csp_options

from tastytrade.dxfeed.summary import Summary
from pandas import DataFrame
from sys import argv, exit
import asyncio
import json



RUN_OPTIONS: dict[str, str|int|float|bool] = {
    "output": "./csp_strategy.xlsx",
    "input": "./stock_lists/wheel.txt",
    "delta": .30,
    "dte" : 45,
    "debug": False
}


def print_help() -> None:
    print("CSP Strategy Generator")
    print("----------------------")
    print("Note: for args be sure to use `--arg=value` format!")
    print("Args and their default values")
    for arg in RUN_OPTIONS:
        print(f"\t--{arg}={RUN_OPTIONS[arg]}")
    
    exit(0)



def parse_args() -> None:
    for arg in argv:
        if (arg == "--help"):
            print_help()

        if ('=' in arg):
            index: str = arg.removeprefix("--").split('=')[0]
            value_s: str = arg.split('=')[1]

            if (index in list(RUN_OPTIONS.keys())):
                RUN_OPTIONS[index] = type(RUN_OPTIONS[index])(value_s)
                


async def main() -> None:
    parse_args()
    await init_runtime()

    if (RUN_OPTIONS['debug']):
        print(json.dumps(RUN_OPTIONS, indent=4))

    # Data
    quote_lookup: dict[str, float] = {}
    csp_strategy: list[CspData] = []

    symbols: list[str] = get_symbols_list(RUN_OPTIONS['input'])

    raw_quotes: list[Summary] = await get_equity_quotes(symbols)
    for quote in raw_quotes:
        quote_lookup[quote.eventSymbol] = quote.dayClosePrice
        
    csp_strategy = await get_csp_options(symbols, RUN_OPTIONS['dte'], RUN_OPTIONS['delta'], quote_lookup)

    df: DataFrame = DataFrame(csp_strategy)
    print(df.head())
    df.to_excel(RUN_OPTIONS['output'], index=False)


asyncio.run(main())
