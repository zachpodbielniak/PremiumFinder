from structures import CspData
from runtime import RUNTIME_CONFIG

from tastytrade.instruments import Option, OptionType, get_option_chain
from tastytrade.streamer import DataStreamer, EventType 
from tastytrade.dxfeed.summary import Summary
from tastytrade.dxfeed.greeks import Greeks 

from datetime import date, timedelta


# Get EQUITY quotes for supplied symbols list
async def get_equity_quotes(symbols: list[str]) -> list:
    
    await RUNTIME_CONFIG.streamer.subscribe(EventType.SUMMARY, symbols)
    quotes = []
    
    async for quote in RUNTIME_CONFIG.streamer.listen():
        quotes.append(quote)
        if (len(quotes) >= len(symbols)):
            break 
    
    return quotes


# Get OPTION quotes for supplied (OPTIONS) symbols list
async def get_option_quotes(option_symbols: list[str]) -> list[Greeks]:
    greeks: list[Greeks] = []

    await RUNTIME_CONFIG.streamer.subscribe(EventType.GREEKS, option_symbols)
    async for greek in RUNTIME_CONFIG.streamer.listen():
        greeks.append(greek)
        if (len(greeks) >= len(option_symbols)):
            break 

    return(greeks)




# Loads from one of the symbol files
def get_symbols_list(filename: str) -> list[str]:
    symbols: list[str] = []
    f = open(filename, mode='r')

    lines = f.readlines()
    for line in lines:
        if (line != ""):
            symbols.append(line.replace('\n', ''))
                        
    return symbols



# Returns the chain, and date of the options chain that is the furthest 
# to expiration but falls within the `dte` limit
def find_date_chain_for_timeframe(chain: dict[date, list[Option]], dte: int) -> tuple[list[Option], date]:
    today: date = date.today()
    last_date: date = today
    last_delta: int = 0
    
    # Iterate over each of the chains dates, which is its key
    # and find the closest to dte, but not over
    for k in chain.keys():
        delta: timedelta = k - today
        if (delta.days <= dte and delta.days > last_delta):
            last_delta = delta.days
            last_date = k 

    return chain[last_date], last_date



# Returns the options info for the option that has the highest delta 
# without going over the limit of the supplied delta
def get_option_within_delta_limit(option_quotes: list[Greeks], delta: float) -> Greeks:
    max_delta: float = 0.0
    max_greek: Greeks = None

    for quote in option_quotes:
        if (quote.delta < 0.0):
            this_delta: float = -1.0 * quote.delta 
        else:
            this_delta: float = quote.delta

        if (this_delta > delta):
            continue 
        
        if (this_delta > max_delta):
            max_delta = this_delta
            max_greek = quote 

    return max_greek



# Returns an array of CspData objects that represent the acquired data that was used to formulation a cash-secured put 
# strategy that meets the input requirements
async def get_csp_options(symbols: list[str], dte: int, delta: float, quote_lookup: dict[str, float]) -> list[CspData]:
    option_lookup: dict[str, dict[str, float]] = {}
    csp_strategy: list[CspData] = []

    for symbol in symbols:
        option_lookup[symbol] = {}

    for symbol in symbols:
        last_trade_price: float = quote_lookup[symbol]
        full_chain: dict[date, list[Option]] = get_option_chain(RUNTIME_CONFIG.session, symbol)
        chain_info: tuple[list[Option], date] = find_date_chain_for_timeframe(full_chain, dte)
        time_chain: list[Option] = chain_info[0]
        chain_date: date = chain_info[1]

        for option in time_chain:
            if (option.option_type == OptionType.CALL):
                continue 
            
            if (float(option.strike_price) >= last_trade_price):
                continue 

            option_lookup[symbol][option.streamer_symbol] = float(option.strike_price)

        option_quotes: list[Greeks] = await get_option_quotes(list(option_lookup[symbol].keys()))

        greek = get_option_within_delta_limit(option_quotes, delta=delta)

        data: CspData = CspData(
            symbol=symbol,
            expiration_date=str(chain_date),
            strike_price=option_lookup[symbol][greek.eventSymbol],
            premium=round(100.0 * greek.price, ndigits=2),
            risk=round((100 * option_lookup[symbol][greek.eventSymbol]) - (100.0 * greek.price), ndigits=2),
            roc=round((100 * greek.price) / (100 * option_lookup[symbol][greek.eventSymbol]), ndigits=4),
            break_even=round((option_lookup[symbol][greek.eventSymbol] - greek.price), ndigits=2),
            iv=round(greek.volatility, ndigits=4),
            delta=round(greek.delta, ndigits=4),
            gamma=round(greek.gamma, ndigits=4),
            theta=round(greek.theta, ndigits=4),
            rho=round(greek.rho, ndigits=4),
            vega=round(greek.vega, ndigits=4)
        )

        csp_strategy.append(data)

    return csp_strategy




