from account_secrets import username, password 
from tastytrade.session import Session
from tastytrade.streamer import DataStreamer
from dataclasses import dataclass, field, replace


@dataclass
class RuntimeConfig:
    session: Session = field(default=None)
    streamer: DataStreamer = field(default=None)


RUNTIME_CONFIG: RuntimeConfig = RuntimeConfig()



async def init_runtime() -> None:
    session: Session = Session(username, password)
    streamer: DataStreamer = await DataStreamer.create(session)

    RUNTIME_CONFIG.session = session 
    RUNTIME_CONFIG.streamer = streamer 

