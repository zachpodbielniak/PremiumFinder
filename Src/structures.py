from dataclasses import dataclass, field


@dataclass(frozen=True)
class CspData:
    symbol: str = field(default_factory=str)
    expiration_date: str = field(default_factory=str)
    strike_price: float = field(default_factory=float)
    premium: float = field(default_factory=float)
    risk: float = field(default_factory=float)
    roc: float = field(default_factory=float)
    break_even: float = field(default_factory=float)
    iv: float = field(default_factory=float)
    delta: float = field(default_factory=float)
    gamma: float = field(default_factory=float)
    theta: float = field(default_factory=float)
    rho: float = field(default_factory=float)
    vega: float = field(default_factory=float)

    